﻿#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int N = 4;
    int array[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << "\n";
    }

    int Day = buf.tm_mday;
    int ElementSum = Day % N;
    int sum = 0;

    for (int j = 0; j < N; j++)
    {
        sum += array[ElementSum][j];
    }

    setlocale(LC_ALL, "Russian");
    std::cout << "Сумма элементов " << ElementSum << ": " << sum << "\n";

    return 0;
}
